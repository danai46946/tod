﻿using System;
using System.Runtime.InteropServices;
using static System.Console;

namespace TestPush
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string message = "Dotnet: Welcome to using .NET Core!";

            if (args.Length > 0)
            {
                message = String.Join(" ", args);
            }

            var reversedString = $"Reversed string: {ReverseString(message)}";
            WriteLine("**Environment**");
            WriteLine($"Platform: .NET Core 2.4");
            WriteLine($"OS: {RuntimeInformation.OSDescription}");
            WriteLine($"OS: windows 10,8,7, xp");
            WriteLine();
        }

        public static string ReverseString(string input)
        {
            var chars = input.ToCharArray();
            Array.Reverse(chars);
            var reversedString = new string(chars);
            return reversedString;
        }
    }
}
