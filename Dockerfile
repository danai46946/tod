FROM microsoft/dotnet:2.2-runtime AS base
WORKDIR /app

FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /src
COPY TestPush/TestPush.csproj TestPush/
RUN dotnet restore TestPush/TestPush.csproj
COPY . .
WORKDIR /src/TestPush
RUN dotnet build TestPush.csproj -c Release -o /app

FROM build AS publish
RUN dotnet publish TestPush.csproj -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "TestPush.dll"]
